import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/index.vue'
import Movie from '../views/movie.vue'
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/movie/:id',
            name: 'movie',
            component: Movie,
        }
    ]
})

export default router

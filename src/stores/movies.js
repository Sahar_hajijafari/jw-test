import {defineStore} from 'pinia'
import axios from "axios";

export const useMoviesStore = defineStore({
    id: 'movies',
    state: () => ({
        moviesList: [],
        genres: {},
        page: 1,
        popularity: 'popularity.desc',
        totalPage: 1,
        totalResults: 0,
        startDate: '',
        endDate: ''
    }),
    getters: {
        getGenres() {
            return this.genres
        }
    },
    actions: {
        getMoviesList(start = null, end = null,reset=false) {
            let url = 'https://api.themoviedb.org/3/discover/movie?api_key=f62f750b70a8ef11dad44670cfb6aa57'
            if (start && end) {
                this.startDate = start
                this.endDate = end
                this.page = 1
            }
            if(reset){
                this.startDate = null
                this.endDate = null
                this.page = 1
            }
            url += `&page=${this.page}`
            url += `&sort_by=${this.popularity}`
            if (this.startDate && this.endDate) {
                url += `&primary_release_date.gte=${this.startDate}&primary_release_date.lte=${this.endDate}`
            }
            axios.get(url)
                .then(({data}) => {
                    this.moviesList = data.results
                    this.totalPage = data.total_pages
                    this.totalResults = data.total_results
                })
                .catch(error => {
                    console.log(error)
                });
        },
        getGenresList() {
            const url = 'https://api.themoviedb.org/3/genre/movie/list?api_key=f62f750b70a8ef11dad44670cfb6aa57'
            axios.get(url)
                .then(({data}) => {
                    data.genres.forEach(({id, name}) => {
                        this.genres[id] = name
                    })
                })
                .catch(error => {
                    console.log(error)
                });
        },
        getMovie(id) {
            return new Promise((resolve, reject) => {
                axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=f62f750b70a8ef11dad44670cfb6aa57`).then(({data}) => {
                    resolve(data)
                }).catch(reject)
            })
        },
        getCredits(id){
            return new Promise((resolve, reject) => {
                axios.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=f62f750b70a8ef11dad44670cfb6aa57`).then(({data}) => {
                    resolve(data)
                }).catch(reject)
            })
        },
        getNextPage() {
            this.page += 1
            this.moviesList=[]
            this.getMoviesList()
        },
        getPreviousPage() {
            this.page -= 1
            this.moviesList=[]
            this.getMoviesList()
        }
    }
})

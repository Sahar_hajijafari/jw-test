/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{vue,js,ts,jsx,tsx}"
    ],
    theme: {
        screens: {
            sm: '480px',
            md: '768px',
            lg: '976px',
            xl: '1024px',
        },
        colors: {
            'green': '#11B980',
            'gray': '#E2E2E2',
            'black': '#000000',
            'blue': '#549DF2',
            'white': '#ffffff',
            'lightGray':'#4E4E4E',
            'lightBlack':'#989898',
            'zinc':'#151515',
            'slate':'#1E1E1E'
        },
        fontSize: {
            large:['18px','22px'],
            big:['18px','21px'],
            base: ['16px', '19px'],
            medium:['16px', '22px'],
            xMedium:['14px', '22px'],
            small: ['12px', '14px']
        },
        extend: {
            spacing: {
                '128': '32rem',
                '144': '36rem',
            },
            borderRadius: {
                '4xl': '2rem',
            }
        }
    },
    plugins: [],
}

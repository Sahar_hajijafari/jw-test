import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import './assets/main.css'
import './assets/index.css'
import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'
import axios from './plugins/axios.js'

const app = createApp(App)

app.component('Datepicker', Datepicker);
app.use(createPinia())
app.use(router)
app.use(axios)
app.mount('#app')
